package es.cic.curso.curso12.mascota.got.servicio;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso12.mascota.got.dominio.Familia;
import es.cic.curso.curso12.mascota.got.dominio.FamiliaRepository;
import es.cic.curso.curso12.mascota.got.dominio.Reino;
import es.cic.curso.curso12.mascota.got.dominio.Familia;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso12/mascota.got/applicationContext.xml"
				})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class FamiliaServiceTest {

	@Autowired
	private FamiliaService sut;
	
	@Autowired
	private FamiliaRepository familiaRepo;
	
	@PersistenceContext
	protected EntityManager em;
	private Reino dorne;
	
	@Before
	public void setUp() throws Exception {
		limpiarFamilias();
		dorne = new Reino("Dorne");
		em.persist(dorne);
		em.flush();
	}

	@Test
	public void testNuevoFamilia() {
		Familia f = new Familia();
		sut.nuevaFamilia(f);
		
		assertNotNull(f.getId()); 	
	}
	
	@Test
	public void testObtenerFamilia(){
		Long f = sut.nuevaFamilia("Martel",dorne,"Unbent.unbroke.unbroken","Sol y lanza",50,false);
		
		Familia familia = sut.obtenerFamilia(f);
		
		assertNotNull(familia.getId());
		assertTrue(familia.getNombre().equals("Martel"));
	}
	
	@Test
	public void testObtenerFamilias(){
		List<Familia> familias = sut.obtenerFamilias();
		for(Familia f:familias){
			assertNotNull(f.getId());
		}
	}
	
	@Test
	public void testActualizarFamilia(){
		Long idF= sut.nuevaFamilia("Martel",dorne,"Unbent.unbroke.unbroken","Sol y lanza",50,false);
		Familia f = sut.obtenerFamilia(idF);
		f.setPoder(40);
		Familia fMod=sut.obtenerFamilia(idF);
		assertTrue(fMod.getPoder()==40);
	}
	
	@Test
	public void testBorrarFamilia(){
		Long idF =  sut.nuevaFamilia("Martel",dorne,"Unbent.unbroke.unbroken","Sol y lanza",50,false);
		sut.eliminarFamilia(idF);
		
		List<Familia> fs = sut.obtenerFamilias();
		assertTrue(fs.size()==0);
	}

	private void limpiarFamilias(){
		List<Familia> familias = sut.obtenerFamilias();
		for(Familia f: familias){
			familiaRepo.delete(f);
		}
	}
}
