package es.cic.curso.curso12.mascota.got.dominio;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso12/mascota.got/applicationContext.xml"
				})
public class MuerteRepositoryImplTest extends AbstractRepositoryImplTest<Long, Muerte>{

	@Autowired
	private MuerteRepository sut;	
	
	private Personaje personaje;
	
	@Before
	public void setUp() throws Exception {
		personaje = new Personaje("Ned Stark");
		
		em.persist(personaje);
	}
	
	
	
	@Override
	public IRepository<Long, Muerte> getRepository() {
		return sut;
	}

	@Override
	public Muerte getInstanceDeTParaNuevo() {
		Muerte muerte = new Muerte();
		muerte.setDescripcion("Ejecutado");
		muerte.setPersonaje(personaje);
		return muerte;
	}

	@Override
	public Muerte getInstanceDeTParaLectura() {
		Muerte muerte = new Muerte();
		muerte.setDescripcion("Ejecutado");
		muerte.setPersonaje(personaje);
		return muerte;
	}

	@Override
	public boolean sonDatosIguales(Muerte t1, Muerte t2) {
		if (t1 == null || t2 == null) {
			throw new UnsupportedOperationException("No puedo comparar nulos");
		}
		if (t1.getPersonaje()!=t1.getPersonaje()){
			return false;
		}
		if (t1.getDescripcion()!=t2.getDescripcion()){
			return false;
		}
		return true;
	}


	@Override
	public Long getClavePrimariaNoExistente() {
		return Long.MAX_VALUE;
	}

	@Override
	public Muerte getInstanceDeTParaModificar(Long clave) {
		Muerte m = getInstanceDeTParaLectura();
		m.setId(clave);
		m.setDescripcion("enfermedad");
		
		return m;
	}

}
