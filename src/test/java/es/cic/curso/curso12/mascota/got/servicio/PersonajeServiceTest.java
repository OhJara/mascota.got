package es.cic.curso.curso12.mascota.got.servicio;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso12.mascota.got.dominio.Familia;
import es.cic.curso.curso12.mascota.got.dominio.Personaje;
import es.cic.curso.curso12.mascota.got.dominio.PersonajeRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso12/mascota.got/applicationContext.xml"
				})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class PersonajeServiceTest {

	@Autowired
	PersonajeService sut;
	
	@PersistenceContext
	EntityManager em;
	private Familia stark;
	@Autowired
	PersonajeRepository personajeRepo;

	
	@Before
	public void setUp() throws Exception {
		limpiarPersonajes();
		stark = new Familia("Stark");
		em.persist(stark);
		em.flush();
	}

	@Test
	public void testNuevoPersonaje() {
		Personaje p = new Personaje();
		sut.nuevoPersonaje(p);
		
		assertNotNull(p.getId()); 	
	}
	
	@Test
	public void testObtenerPersonaje(){
		Long p = sut.nuevoPersonaje("Jon",stark,68,43,65,true,"nightWatch");
		
		Personaje personaje = sut.obtenerPersonaje(p);
		
		assertNotNull(personaje.getId());
		assertTrue(personaje.getNombre().equals("Jon"));
	}
	
	@Test
	public void testObtenerPersonajes(){
		List<Personaje> personajes = sut.obtenerPersonajes();
		for(Personaje p:personajes){
			assertNotNull(p.getId());
		}
	}
	
	@Test
	public void testActualizarPersonaje(){
		Long idP=sut.nuevoPersonaje("Jon",stark,68,43,65,true,"nightWatch");
		Personaje p = sut.obtenerPersonaje(idP);
		p.setCombate(10);
		
		Personaje pMod=sut.obtenerPersonaje(idP);
		assertTrue(pMod.getCombate()==10);
	}
	
	@Test
	public void testBorrarPersonaje(){
		Long idP = sut.nuevoPersonaje("Jon",stark,68,43,65,true,"nightWatch");
		sut.eliminarPersonaje(idP);
		
		List<Personaje> ps = sut.obtenerPersonajes();
		assertTrue(ps.size()==0);
	}
	
	private void limpiarPersonajes(){
		List<Personaje> personajes = sut.obtenerPersonajes();
		for(Personaje p: personajes){
			personajeRepo.delete(p);
		}
	}

}
