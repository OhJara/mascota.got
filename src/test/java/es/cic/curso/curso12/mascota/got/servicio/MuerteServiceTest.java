package es.cic.curso.curso12.mascota.got.servicio;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso12.mascota.got.dominio.Muerte;
import es.cic.curso.curso12.mascota.got.dominio.MuerteRepository;
import es.cic.curso.curso12.mascota.got.dominio.Personaje;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso12/mascota.got/applicationContext.xml"
				})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class MuerteServiceTest {

	@Autowired
	MuerteService sut;
	
	@Autowired
	MuerteRepository muerteRepo;
	
	@PersistenceContext
	protected EntityManager em;
	private Personaje ned;

	
	@Before
	public void setUp() throws Exception {
		limpiarMuertes();
		ned = new Personaje("Ned");
		em.persist(ned);
		em.flush();
	}

	@Test
	public void testNuevoMuerte() {
		Muerte p = new Muerte();
		sut.nuevaMuerte(p);
		
		assertNotNull(p.getId()); 	
	}
	
	@Test
	public void testObtenerMuerte(){
		Long p = sut.nuevaMuerte("Cabeza fuera", ned);
		
		Muerte muerte = sut.obtenerMuerte(p);
		
		assertNotNull(muerte.getId());
		assertTrue(muerte.getPersonaje().equals(ned));
	}
	
	@Test
	public void testObtenerMuertes(){
		List<Muerte> muertes = sut.obtenerMuertes();
		for(Muerte p:muertes){
			assertNotNull(p.getId());
		}
	}
	
	@Test
	public void testActualizarMuerte(){
		Long idP=sut.nuevaMuerte("Out", ned);
		Muerte p = sut.obtenerMuerte(idP);
		p.setDescripcion("ahogado");
		
		Muerte pMod=sut.obtenerMuerte(idP);
		assertTrue(pMod.getDescripcion().equals("ahogado"));
	}
	
	@Test
	public void testBorrarMuerte(){
		Long idP = sut.nuevaMuerte("sin cabeza", ned);
		sut.eliminarMuerte(idP);
		
		List<Muerte> ps = sut.obtenerMuertes();
		assertTrue(ps.size()==0);
	}
	
	private void limpiarMuertes(){
		List<Muerte> muertes = sut.obtenerMuertes();
		for(Muerte p: muertes){
			muerteRepo.delete(p);
		}
	}

}

