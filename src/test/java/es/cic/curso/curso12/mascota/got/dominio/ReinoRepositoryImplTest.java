package es.cic.curso.curso12.mascota.got.dominio;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso12/mascota.got/applicationContext.xml"
				})
public class ReinoRepositoryImplTest extends AbstractRepositoryImplTest<Long, Reino>  {

	@Autowired
	private ReinoRepository sut;
	
	private Familia stark;
	
	@Before
	public void setUp() throws Exception {		
		stark = new Familia("Stark");
		em.persist(stark);
	}
	
	
	@Override
	public IRepository<Long, Reino> getRepository() {
		return sut;
	}

	@Override
	public Reino getInstanceDeTParaNuevo() {
		Reino r = new Reino();
		r.setCapital("Invernalia");
		r.setNombre("El Norte");
		r.setFamiliaPpl(stark);
	
		return r;
	}

	@Override
	public Reino getInstanceDeTParaLectura() {
		Reino r = new Reino();
		r.setCapital("Invernalia");
		r.setNombre("El Norte");
		r.setFamiliaPpl(stark);
	
		return r;
	}

	@Override
	public boolean sonDatosIguales(Reino t1, Reino t2) {
		if (t1 == null || t2 == null) {
			throw new UnsupportedOperationException("No puedo comparar nulos");
		}
		if (t1.getNombre()!=t2.getNombre()){
			return false;
		}
		if(t1.getCapital()!=t2.getCapital()){
			return false;
		}
		if(t1.getFamiliaPpl()!=t2.getFamiliaPpl()){
			return false;
		}
		return true;
	}

	@Override
	public Long getClavePrimariaNoExistente() {
		return Long.MAX_VALUE;
	}

	@Override
	public Reino getInstanceDeTParaModificar(Long clave) {
		Reino r = getInstanceDeTParaLectura();
		
		r.setId(clave);
		r.setNombre("La Roca");
		return r;
	}
	

}
