package es.cic.curso.curso12.mascota.got.dominio;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso12/mascota.got/applicationContext.xml"
				})
public class PersonajeRepositorioImplTest extends AbstractRepositoryImplTest<Long, Personaje> {

	@Autowired
	private PersonajeRepository sut;	
	
	private Familia stark;
	
	@Before
	public void setUp() throws Exception {
		stark = new Familia("Stark");
		
		em.persist(stark);
	}
	
	@Override
	public Personaje getInstanceDeTParaNuevo() {
		Personaje personaje = new Personaje();
		
		personaje.setNombre("Jon Nieve");
		personaje.setAyudante(true);
		personaje.setCombate(80);
		personaje.setPersuasion(60);
		personaje.setAfiliacion("Guardia de la noche");
		personaje.setFamilia(stark);
		
		
		return personaje;
	}

	@Override
	public Personaje getInstanceDeTParaLectura() {
		Personaje p = new Personaje();
		
		p.setNombre("Daenerys Targaryen");
		p.setAyudante(true);
		p.setPersuasion(80);
		p.setRiqueza(70);
		p.setCombate(40);
		
		return p;
	}

	@Override
	public boolean sonDatosIguales(Personaje p1 , Personaje p2) {
		if (p1 == null || p2 == null) {
			throw new UnsupportedOperationException("No puedo comparar nulos");
		}
		if (p1.getAfiliacion() != p2.getAfiliacion()) {
			return false;
		}
		if (p1.getCombate() != p2.getCombate()) {
			return false;
		}	
		if (p1.getRiqueza() != p2.getRiqueza()) {
			return false;
		}
		if (p1.getPersuasion()!= p2.getPersuasion()) {
			return false;
		}
		if (p1.getNombre()!= p2.getNombre()){
			return false;
		}
		if(p1.getRiqueza()!=p2.getRiqueza()){
			return false;
		}
		if(p1.getFamilia()!=p2.getFamilia()){
			return false;
		}
		
		return true;
	}

	@Override
	public Long getClavePrimariaNoExistente() {
		return Long.MAX_VALUE;
	}

	@Override
	public Personaje getInstanceDeTParaModificar(Long clave) {
		Personaje p = getInstanceDeTParaLectura();
		p.setId(clave);
		p.setNombre("Tyrion");
		return p;
	}

	@Override
	public IRepository<Long, Personaje> getRepository() {
		return sut;
	}	


}

