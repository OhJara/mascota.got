package es.cic.curso.curso12.mascota.got.dominio;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso12/mascota.got/applicationContext.xml"
				})
public class FamiliaRepositoryImplTest extends AbstractRepositoryImplTest<Long, Familia>{

	@Autowired
	private FamiliaRepository sut;
	
	private Reino norte;
	
	@Before
	public void setUp() throws Exception {
		norte = new Reino("El Norte");
		
		em.persist(norte);
	}
	
	
	@Override
	public IRepository<Long, Familia> getRepository() {
		return sut;
	}

	@Override
	public Familia getInstanceDeTParaNuevo() {
		Familia familia = new Familia();
		familia.setBlason("Lobo Huargo");
		familia.setNombre("Stark");
		familia.setPoder(70);
		familia.setLema("Winter is coming");
		familia.setReino(norte);
		
		
		return familia;
	}

	@Override
	public Familia getInstanceDeTParaLectura() {
		Familia f = new Familia();
		f.setNombre("Martel");
		f.setLema("Unbent. Unbowed. Unbroken");
		f.setBlason("Sol atravesado de lanza");
		f.setPoder(40);		
		
		return f;
	}

	@Override
	public boolean sonDatosIguales(Familia p1, Familia p2) {
		if (p1 == null || p2 == null) {
			throw new UnsupportedOperationException("No puedo comparar nulos");
		}
		if(!p1.getBlason().equals(p2.getBlason())){
			return false;
		}
		if(!p1.getLema().equals(p2.getLema())){
			return false;
		}
		if(!p1.getNombre().equals(p2.getNombre())){
			return false;
		}
		if(p1.getPoder()!=p2.getPoder()){
			return false;
		}
		if(p2.getReino()!=p2.getReino()){
			return false;
		}
		
		return true;
	}

	@Override
	public Long getClavePrimariaNoExistente() {
		return Long.MAX_VALUE;
	}

	@Override
	public Familia getInstanceDeTParaModificar(Long clave) {
		Familia f = getInstanceDeTParaLectura();
		f.setId(clave);
		f.setNombre("Greyjoy");
		return f;
	}
	

}
