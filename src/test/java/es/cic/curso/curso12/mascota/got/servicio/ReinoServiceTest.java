package es.cic.curso.curso12.mascota.got.servicio;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso12.mascota.got.dominio.Reino;
import es.cic.curso.curso12.mascota.got.dominio.ReinoRepository;
import es.cic.curso.curso12.mascota.got.dominio.Familia;
import es.cic.curso.curso12.mascota.got.dominio.Personaje;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:es/cic/curso/curso12/mascota.got/applicationContext.xml"
				})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class ReinoServiceTest {

	@Autowired
	ReinoService sut;
	
	@Autowired
	ReinoRepository reinoRepo;
	
	@PersistenceContext
	protected EntityManager em;
	private Familia stark;

	
	@Before
	public void setUp() throws Exception {
		limpiarReinos();
		stark = new Familia("Stark");
		em.persist(stark);
		em.flush();
	}

	@Test
	public void testNuevoReino() {
		Reino p = new Reino();
		sut.nuevaReino(p);
		
		assertNotNull(p.getId()); 	
	}
	
	@Test
	public void testObtenerReino(){
		Long p = sut.nuevaReino("Norte","Invernalia",stark);
		
		Reino reino = sut.obtenerReino(p);
		
		assertNotNull(reino.getId());
		assertTrue(reino.getFamiliaPpl().equals(stark));
	}
	
	@Test
	public void testObtenerReinos(){
		List<Reino> reinos = sut.obtenerReinos();
		for(Reino p:reinos){
			assertNotNull(p.getId());
		}
	}
	
	@Test
	public void testActualizarReino(){
		Long idP = sut.nuevaReino("Norte","Invernalia",stark);		
		Reino p = sut.obtenerReino(idP);
		p.setCapital("Fuerte Terror");
		
		Reino pMod=sut.obtenerReino(idP);
		assertTrue(pMod.getCapital().equals("Fuerte Terror"));
	}
	
	@Test
	public void testBorrarReino(){
		Long idP = sut.nuevaReino("Norte","Invernalia",stark);		
		sut.eliminarReino(idP);
		
		List<Reino> ps = sut.obtenerReinos();
		assertTrue(ps.size()==0);
	}
	
	private void limpiarReinos(){
		List<Reino> reinos = sut.obtenerReinos();
		for(Reino p: reinos){
			reinoRepo.delete(p);
		}
	}

}

