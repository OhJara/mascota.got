package es.cic.curso.curso12.mascota.got.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Muerte implements Identificable<Long>{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@JoinColumn(name="personaje_id")
	@OneToOne(fetch=FetchType.LAZY)
	private Personaje personaje_id;
	@Column(name="descripcion")
	private String descripcion;
	
	public Muerte(){
		super();
	}
	
	public Muerte(Personaje personaje){
		super();
		this.personaje_id=personaje;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Personaje getPersonaje() {
		return personaje_id;
	}
	public void setPersonaje(Personaje personaje) {
		this.personaje_id = personaje;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Muerte other = (Muerte) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Muerte [id=" + id + ", personaje=" + personaje_id + ", descripcion=" + descripcion + "]";
	}
	
	
}
