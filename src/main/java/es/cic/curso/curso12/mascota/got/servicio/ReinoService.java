package es.cic.curso.curso12.mascota.got.servicio;

import java.util.List;

import es.cic.curso.curso12.mascota.got.dominio.Familia;
import es.cic.curso.curso12.mascota.got.dominio.Reino;

public interface ReinoService {

	Reino nuevaReino(Reino reino);

	Long nuevaReino(String nombre, String capital, Familia familia);

	Reino obtenerReino(Long id);

	List<Reino> obtenerReinos();

	Reino actualizarReino(Reino modificada);

	void eliminarReino(Long id);

}