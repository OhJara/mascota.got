package es.cic.curso.curso12.mascota.got.dominio;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class PersonajeRepositoryImpl extends AbstractRepositoryImpl<Long, Personaje> implements PersonajeRepository {

	@Override
	public Class<Personaje> getClassDeT() {
		return Personaje.class;
	}

	@Override
	public String getNombreTabla() {
		return "personaje";
	}

}
