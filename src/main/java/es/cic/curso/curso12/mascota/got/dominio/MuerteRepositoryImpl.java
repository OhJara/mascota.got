package es.cic.curso.curso12.mascota.got.dominio;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class MuerteRepositoryImpl extends AbstractRepositoryImpl<Long, Muerte> implements MuerteRepository{

	@Override
	public Class<Muerte> getClassDeT() {
		return Muerte.class;
	}

	@Override
	public String getNombreTabla() {
		return "muerte";
	}

}
