package es.cic.curso.curso12.mascota.got.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso12.mascota.got.dominio.Familia;
import es.cic.curso.curso12.mascota.got.dominio.FamiliaRepository;
import es.cic.curso.curso12.mascota.got.dominio.Personaje;
import es.cic.curso.curso12.mascota.got.dominio.PersonajeRepository;

@Service
@Transactional
public class PersonajeServiceImpl implements PersonajeService {
	@Autowired
	PersonajeRepository personajeRepo;
	@Autowired
	FamiliaRepository familiaRepo;
	
	@Override
	public Personaje nuevoPersonaje(Personaje nuevo){
		return personajeRepo.add(nuevo);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.PersonajeService#nuevoPersonaje(java.lang.String, es.cic.curso.curso12.mascota.got.dominio.Familia, int, int, int, boolean, java.lang.String)
	 */
	@Override
	public Long nuevoPersonaje(String nombre, Familia familia, int combate, int riqueza, int persuasion,
			boolean ayudante, String afiliacion){
		Personaje personaje = new Personaje();
		
		personaje.setNombre(nombre);
		personaje.setFamilia(familia);
		personaje.setCombate(combate);
		personaje.setRiqueza(riqueza);
		personaje.setRiqueza(riqueza);
		personaje.setPersuasion(persuasion);
		personaje.setAyudante(ayudante);
		personaje.setAfiliacion(afiliacion);
		
		Personaje nuevo = nuevoPersonaje(personaje);
		
		return nuevo.getId();		
	}

	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.PersonajeService#obtenerPersonaje(java.lang.Long)
	 */
	@Override
	public Personaje obtenerPersonaje(Long id){
		return personajeRepo.read(id);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.PersonajeService#obtenerPersonajes()
	 */
	@Override
	public List<Personaje> obtenerPersonajes(){
		return personajeRepo.list();
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.PersonajeService#actualizarPersonaje(es.cic.curso.curso12.mascota.got.dominio.Personaje)
	 */
	@Override
	public Personaje actualizarPersonaje(Personaje modificado){
		return personajeRepo.update(modificado);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.PersonajeService#eliminarPersonaje(java.lang.Long)
	 */
	@Override
	public void eliminarPersonaje(Long id){
		Personaje aEliminar = obtenerPersonaje(id);
		personajeRepo.delete(id);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
