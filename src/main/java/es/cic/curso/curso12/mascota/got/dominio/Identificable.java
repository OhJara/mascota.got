package es.cic.curso.curso12.mascota.got.dominio;

import java.io.Serializable;

public interface Identificable<K> extends Serializable {

	K getId();

	void setId(K id);

}