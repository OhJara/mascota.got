package es.cic.curso.curso12.mascota.got.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso12.mascota.got.dominio.Familia;
import es.cic.curso.curso12.mascota.got.dominio.FamiliaRepository;
import es.cic.curso.curso12.mascota.got.dominio.Reino;
import es.cic.curso.curso12.mascota.got.dominio.ReinoRepository;

@Service
@Transactional
public class FamiliaServiceImpl implements FamiliaService {

	@Autowired
	private FamiliaRepository familiaRepo;
	@Autowired
	private ReinoRepository reinoRepo;
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.FamiliaService#nuevaFamilia(es.cic.curso.curso12.mascota.got.dominio.Familia)
	 */
	@Override
	public Familia nuevaFamilia(Familia nueva){
		return familiaRepo.add(nueva);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.FamiliaService#nuevaFamilia(java.lang.String, es.cic.curso.curso12.mascota.got.dominio.Reino, java.lang.String, java.lang.String, int, boolean)
	 */
	@Override
	public Long nuevaFamilia(String nombre, Reino reino, String lema, String blason, int poder, boolean vasaya){
		Familia familia = new Familia();
		
		familia.setNombre(blason);
		familia.setNombre(nombre);
		familia.setPoder(poder);
		familia.setReino(reino);
		familia.setVasaya(vasaya);
		familia.setLema(lema);
		
		Familia nueva = nuevaFamilia(familia);
		return nueva.getId();
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.FamiliaService#obtenerFamilia(java.lang.Long)
	 */
	@Override
	public Familia obtenerFamilia(Long id){
		return familiaRepo.read(id);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.FamiliaService#obtenerFamilias()
	 */
	@Override
	public List<Familia> obtenerFamilias(){
		return familiaRepo.list();
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.FamiliaService#actualizarFamilia(es.cic.curso.curso12.mascota.got.dominio.Familia)
	 */
	@Override
	public Familia actualizarFamilia(Familia modificada){
		return familiaRepo.update(modificada);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.FamiliaService#eliminarFamilia(java.lang.Long)
	 */
	@Override
	public void eliminarFamilia(Long id){
		Familia aEliminar = obtenerFamilia(id);
		familiaRepo.delete(id);
	}

}

