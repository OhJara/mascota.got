package es.cic.curso.curso12.mascota.got.dominio;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class FamiliaRepositoryImpl extends AbstractRepositoryImpl<Long, Familia> implements FamiliaRepository{

	@Override
	public Class<Familia> getClassDeT() {
		return Familia.class;
	}

	@Override
	public String getNombreTabla() {
		return "familia";
	}

}
