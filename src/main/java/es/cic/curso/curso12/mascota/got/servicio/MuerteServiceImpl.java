package es.cic.curso.curso12.mascota.got.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso12.mascota.got.dominio.Muerte;
import es.cic.curso.curso12.mascota.got.dominio.Muerte;
import es.cic.curso.curso12.mascota.got.dominio.MuerteRepository;
import es.cic.curso.curso12.mascota.got.dominio.Personaje;
import es.cic.curso.curso12.mascota.got.dominio.PersonajeRepository;

@Service
@Transactional
public class MuerteServiceImpl implements MuerteService {
	@Autowired
	private MuerteRepository muerteRepo;
	@Autowired
	private PersonajeRepository personajeRepo;
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.MuerteService#nuevaMuerte(es.cic.curso.curso12.mascota.got.dominio.Muerte)
	 */
	@Override
	public Muerte nuevaMuerte(Muerte muerte){
		return muerteRepo.add(muerte);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.MuerteService#nuevaMuerte(java.lang.String, es.cic.curso.curso12.mascota.got.dominio.Personaje)
	 */
	@Override
	public Long nuevaMuerte(String descripcion, Personaje personaje){
		Muerte muerte= new Muerte();
		muerte.setDescripcion(descripcion);
		muerte.setPersonaje(personaje);
		
		Muerte nueva = nuevaMuerte(muerte);
		return nueva.getId();
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.MuerteService#obtenerMuerte(java.lang.Long)
	 */
	@Override
	public Muerte obtenerMuerte(Long id){
		return muerteRepo.read(id);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.MuerteService#obtenerMuertes()
	 */
	@Override
	public List<Muerte> obtenerMuertes(){
		return muerteRepo.list();
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.MuerteService#actualizarMuerte(es.cic.curso.curso12.mascota.got.dominio.Muerte)
	 */
	@Override
	public Muerte actualizarMuerte(Muerte modificada){
		return muerteRepo.update(modificada);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.MuerteService#eliminarMuerte(java.lang.Long)
	 */
	@Override
	public void eliminarMuerte(Long id){
		Muerte aEliminar = obtenerMuerte(id);
		muerteRepo.delete(id);
	}
}


