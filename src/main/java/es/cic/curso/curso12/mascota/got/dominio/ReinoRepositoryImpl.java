package es.cic.curso.curso12.mascota.got.dominio;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class ReinoRepositoryImpl extends AbstractRepositoryImpl<Long, Reino> implements ReinoRepository{

	@Override
	public Class<Reino> getClassDeT() {
		return Reino.class;
	}

	@Override
	public String getNombreTabla() {
		return "reino";
	}

}
