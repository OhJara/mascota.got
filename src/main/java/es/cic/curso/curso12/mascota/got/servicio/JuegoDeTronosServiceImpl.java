package es.cic.curso.curso12.mascota.got.servicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso12.mascota.got.dominio.Personaje;

@Service
@Transactional
public class JuegoDeTronosServiceImpl {

	@Autowired
	private PersonajeService personajeService;
	@Autowired
	private FamiliaService familiaService;
	@Autowired
	private ReinoService reinoService;
	@Autowired
	private MuerteService muerteService;
	
	
	/**
	 * dos personajes combate, tener ayudante da un plus de 10 a la habilidad en combate
	 * el ganador recibe 5pts de combate más, el perdedor intenta persuadir al ganador 
	 * para no morir, si lo consigue pierde habilidad de combate y el ayudante, si no muere.
	 * Si pierde el ayudante no se resetea el valor de combate. Esto se atribuye al 
	 * concepto furia.
	 * @param personaje1
	 * @param personaje2
	 * @return
	 */
	public Personaje combate(Personaje personaje1, Personaje personaje2){
		hasAyudante(personaje1);
		hasAyudante(personaje2);
		Personaje ganador = new Personaje();
		if(personaje1.getCombate()>personaje2.getCombate()){
			descansarAyudante(personaje1);
			personaje1.setCombate(personaje1.getCombate()+5);
			ganador = personaje1;
			if(!persuadir(personaje2,personaje1)){
				muerteService.nuevaMuerte("Muerte por combate",personaje2);
				personajeService.eliminarPersonaje(personaje2.getId());
			}else{
				personaje2.setAyudante(false);
				personaje2.setCombate(personaje2.getCombate()-5);
			}
		}else{
			descansarAyudante(personaje2);
			personaje2.setCombate(personaje2.getCombate()+5);
			ganador = personaje2;
			if(!persuadir(personaje1,personaje2)){
				muerteService.nuevaMuerte("Muerte por combate",personaje1);
				personajeService.eliminarPersonaje(personaje1.getId());
			}else{
				personaje1.setAyudante(false);
				personaje1.setCombate(personaje1.getCombate()-5);
			}
		}
		return ganador;
	}	
	
	/**
	 * comprobamos si un personaje tiene ayudante, si lo tiene añadimos habilidad de combate
	 * @param personaje
	 */
	
	private void hasAyudante(Personaje personaje){
		int poder = personaje.getCombate();
		if(personaje.isAyudante()){
			personaje.setCombate(poder+10);
		}
	}
	
	/**
	 * si un personaje tiene ayudante
	 * Despues de un combate devolvemos la habildiad de combate del personaje a su estado
	 * natural
	 */
	private void descansarAyudante(Personaje personaje){
		if(personaje.isAyudante()){			
			int poder = personaje.getCombate();
			personaje.setCombate(poder-10);
		}
	}
	/**
	 * El persuasor intenta persuadir al persuadido.
	 * Si no lo consigue siempre puede comprarlo. La diferencia de riqueza debe ser de
	 * 70, siendo el total 100 y el minimo 0.
	 */
	public boolean persuadir(Personaje persuasor, Personaje persuadido){
		if(persuasor.getPersuasion()>persuadido.getPersuasion()){
			return true;
		}else{
			if(sobornar(persuasor,persuadido,70)){
				return true;				
			}else{
				return false;
			}
		}
	}
	
	/**
	 * mas que comprador, aqui sería sobornador. Segun la diferencia de riqueza un personaje
	 * puede comprar a otro. El precio dicta la diferencia entre riquezas de los personajes
	 * @param comprador
	 * @param personaje
	 * @return
	 */
	public boolean sobornar(Personaje comprador, Personaje personaje,int precio){
		int diferencia =comprador.getRiqueza()-personaje.getRiqueza();
		if(diferencia>=precio){
			return true;
		}else{
			return false;
		}
	}
	
		
}
