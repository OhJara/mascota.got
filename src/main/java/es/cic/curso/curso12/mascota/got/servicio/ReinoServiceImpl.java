package es.cic.curso.curso12.mascota.got.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.curso.curso12.mascota.got.dominio.Reino;
import es.cic.curso.curso12.mascota.got.dominio.ReinoRepository;
import es.cic.curso.curso12.mascota.got.dominio.Familia;
import es.cic.curso.curso12.mascota.got.dominio.FamiliaRepository;


@Service
@Transactional
public class ReinoServiceImpl implements ReinoService {
	@Autowired
	private ReinoRepository reinoRepo;
	@Autowired
	private FamiliaRepository familiaRepo;
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.ReinoService#nuevaReino(es.cic.curso.curso12.mascota.got.dominio.Reino)
	 */
	@Override
	public Reino nuevaReino(Reino reino){
		return reinoRepo.add(reino);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.ReinoService#nuevaReino(java.lang.String, java.lang.String, es.cic.curso.curso12.mascota.got.dominio.Familia)
	 */
	@Override
	public Long nuevaReino(String nombre, String capital, Familia familia){
		Reino reino= new Reino();
		reino.setCapital(capital);
		reino.setFamiliaPpl(familia);
		reino.setNombre(nombre);
		
		Reino nueva = nuevaReino(reino);
		return nueva.getId();
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.ReinoService#obtenerReino(java.lang.Long)
	 */
	@Override
	public Reino obtenerReino(Long id){
		return reinoRepo.read(id);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.ReinoService#obtenerReinos()
	 */
	@Override
	public List<Reino> obtenerReinos(){
		return reinoRepo.list();
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.ReinoService#actualizarReino(es.cic.curso.curso12.mascota.got.dominio.Reino)
	 */
	@Override
	public Reino actualizarReino(Reino modificada){
		return reinoRepo.update(modificada);
	}
	
	/* (non-Javadoc)
	 * @see es.cic.curso.curso12.mascota.got.servicio.ReinoService#eliminarReino(java.lang.Long)
	 */
	@Override
	public void eliminarReino(Long id){
		Reino aEliminar = obtenerReino(id);
		reinoRepo.delete(id);
	}
}


