package es.cic.curso.curso12.mascota.got.servicio;

import java.util.List;

import es.cic.curso.curso12.mascota.got.dominio.Familia;
import es.cic.curso.curso12.mascota.got.dominio.Personaje;

public interface PersonajeService {

	Personaje nuevoPersonaje(Personaje personaje);
	
	Long nuevoPersonaje(String nombre, Familia familia, int combate, int riqueza, int persuasion, boolean ayudante,
			String afiliacion);
	
	Personaje obtenerPersonaje(Long id);
	
	List<Personaje> obtenerPersonajes();

	Personaje actualizarPersonaje(Personaje modificado);

	void eliminarPersonaje(Long id);

}