package es.cic.curso.curso12.mascota.got.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Reino implements Identificable<Long>{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="nombre")
	private String nombre;
	@Column(name="capital")
	private String capital;
	@JoinColumn(name="familia_ppl")
	@OneToOne(fetch=FetchType.LAZY)
	private Familia familia_ppl;
	
	public Reino(){
		super();
	}
	
	public Reino(String nombre, String capital, Familia familiaPpl){
		super();
		this.nombre=nombre;
		this.capital=capital;
		this.familia_ppl=familiaPpl;
	}
	
	public Reino(String nombre){
		super();
		this.nombre=nombre;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCapital() {
		return capital;
	}
	public void setCapital(String capital) {
		this.capital = capital;
	}
	public Familia getFamiliaPpl() {
		return familia_ppl;
	}
	public void setFamiliaPpl(Familia familiaPpl) {
		this.familia_ppl = familiaPpl;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reino other = (Reino) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Reino [id=" + id + ", nombre=" + nombre + ", capital=" + capital + ", familiaPpl=" + familia_ppl + "]";
	}
	
	
	
}
