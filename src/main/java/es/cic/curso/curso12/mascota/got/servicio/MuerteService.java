package es.cic.curso.curso12.mascota.got.servicio;

import java.util.List;

import es.cic.curso.curso12.mascota.got.dominio.Muerte;
import es.cic.curso.curso12.mascota.got.dominio.Personaje;

public interface MuerteService {

	Muerte nuevaMuerte(Muerte muerte);

	Long nuevaMuerte(String descripcion, Personaje personaje);

	Muerte obtenerMuerte(Long id);

	List<Muerte> obtenerMuertes();

	Muerte actualizarMuerte(Muerte modificada);

	void eliminarMuerte(Long id);

}