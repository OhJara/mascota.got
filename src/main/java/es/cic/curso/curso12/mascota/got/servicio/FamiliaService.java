package es.cic.curso.curso12.mascota.got.servicio;

import java.util.List;

import es.cic.curso.curso12.mascota.got.dominio.Familia;
import es.cic.curso.curso12.mascota.got.dominio.Reino;

public interface FamiliaService {

	Familia nuevaFamilia(Familia nueva);

	Long nuevaFamilia(String nombre, Reino reino, String lema, String blason, int poder, boolean vasaya);
	
	Familia obtenerFamilia(Long id);

	List<Familia> obtenerFamilias();

	Familia actualizarFamilia(Familia modificada);

	void eliminarFamilia(Long id);

}