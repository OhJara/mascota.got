package es.cic.curso.curso12.mascota.got.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Personaje implements Identificable<Long>{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="nombre")
	private String nombre;
	@JoinColumn(name="familia_id")
	@ManyToOne(fetch=FetchType.LAZY)
	private Familia familia_id;
	@Column(name="combate")
	private int combate;
	@Column(name="persuasion")
	private int persuasion;
	@Column(name="riqueza")
	private int riqueza;
	@Column(name="afiliacion")
	private String afiliacion;
	@Column(name="ayudante")
	private boolean ayudante;
	
	public Personaje(){
		super();
	}
	
	public Personaje(String nombre, Familia familia){
		super();
		this.nombre=nombre;
		this.familia_id=familia;
	}
	
	public Personaje(String nombre, Familia familia, int combate, int persuasion,int riqueza){
		super();
		this.nombre=nombre;
		this.familia_id=familia;
		this.combate=combate;
		this.persuasion=persuasion;
		this.riqueza=riqueza;
	}
	
	public Personaje(String nombre){
		super();
		this.nombre=nombre;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Familia getFamilia() {
		return familia_id;
	}
	public void setFamilia(Familia familia) {
		this.familia_id = familia;
	}
	public int getCombate() {
		return combate;
	}
	public void setCombate(int combate) {
		this.combate = combate;
	}
	public int getPersuasion() {
		return persuasion;
	}
	public void setPersuasion(int persuasion) {
		this.persuasion = persuasion;
	}
	public int getRiqueza() {
		return riqueza;
	}
	public void setRiqueza(int riqueza) {
		this.riqueza = riqueza;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	public boolean isAyudante() {
		return ayudante;
	}
	public void setAyudante(boolean ayudante) {
		this.ayudante = ayudante;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personaje other = (Personaje) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Personaje [id=" + id + ", nombre=" + nombre + ", familia=" + familia_id + ", combate=" + combate
				+ ", persuasion=" + persuasion + ", riqueza=" + riqueza + ", afiliacion=" + afiliacion + ", ayudante="
				+ ayudante + "]";
	}
	
	
}
