package es.cic.curso.curso12.mascota.got.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Familia implements Identificable<Long> {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="nombre")
	private String nombre;
	@JoinColumn(name="reino_id")
	@ManyToOne(fetch=FetchType.LAZY)
	private Reino reino_id;
	@Column(name="lema")
	private String lema;
	@Column(name="vasaya")
	private boolean vasaya;
	@Column(name="blason")
	private String blason;
	private int poder;
	
	public Familia(){
		super();
	}
	
	public Familia(String nombre, Reino reino, String lema, String blason){
		super();
		this.nombre=nombre;
		this.reino_id=reino;
		this.lema=lema;
		this.blason=blason;
	}
	
	public Familia(String nombre){
		super();
		this.nombre=nombre;
	}
	
	public Familia(String nombre, Reino reino, String lema, String blason, int poder){
		super();
		this.nombre=nombre;
		this.reino_id=reino;
		this.lema=lema;
		this.blason=blason;
		this.poder=poder;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Reino getReino() {
		return reino_id;
	}
	public void setReino(Reino reino) {
		this.reino_id = reino;
	}
	public String getLema() {
		return lema;
	}
	public void setLema(String lema) {
		this.lema = lema;
	}
	public boolean isVasaya() {
		return vasaya;
	}
	public void setVasaya(boolean vasaya) {
		this.vasaya = vasaya;
	}
	public String getBlason() {
		return blason;
	}
	public void setBlason(String blason) {
		this.blason = blason;
	}
	public int getPoder() {
		return poder;
	}
	public void setPoder(int poder) {
		this.poder = poder;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Familia other = (Familia) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Familia [id=" + id + ", nombre=" + nombre + ", reino=" + reino_id + ", lema=" + lema + ", vasaya=" + vasaya
				+ ", blason=" + blason + ", poder=" + poder + "]";
	}
	
	
}
